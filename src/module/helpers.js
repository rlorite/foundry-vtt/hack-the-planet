export class HTPHelpers {
  /**
   * Identifies duplicate items by type and returns a array of item ids to remove
   *
   * @param {Object} item_data
   * @param {Document} actor
   * @returns {Array}
   *
   */
  static removeDuplicatedItemType(item_data, actor) {
    const dupe_list = []
    const distinct_types = ['crew_reputation', 'class', 'vice', 'background', 'heritage']
    const allowed_types = ['item']
    const should_be_distinct = distinct_types.includes(item_data.type)
    // If the Item has the exact same name - remove it from list.
    // Remove Duplicate items from the array.
    actor.items.forEach((i) => {
      const has_double = item_data.type === i.type
      if (
        (i.name === item_data.name || (should_be_distinct && has_double)) &&
        !allowed_types.includes(item_data.type) &&
        item_data._id !== i.id
      ) {
        dupe_list.push(i.id)
      }
    })

    return dupe_list
  }

  /**
   * Get the list of all available ingame items by Type.
   *
   * @param {string} item_type
   */
  static async getAllItemsByType(item_type) {
    let list_of_items = []
    let game_items = []
    let compendium_items = []

    game_items = game.items.filter((e) => e.type === item_type)

    const pack = game.packs.find((e) => e.metadata.name === item_type)
    if (pack) {
      compendium_items = await pack.getDocuments()
    }

    list_of_items = game_items.concat(compendium_items)
    list_of_items.sort(function (a, b) {
      const nameA = a.name.toUpperCase()
      const nameB = b.name.toUpperCase()
      return nameA.localeCompare(nameB)
    })
    return list_of_items
  }

  /* -------------------------------------------- */

  /**
   * Returns the label for attribute.
   *
   * @param {string} attribute_name
   * @returns {string}
   */
  static getAttributeLabel(attribute_name) {
    const attributes = CONFIG.HTP.ATTRIBUTES
    const actions = CONFIG.HTP.ACTIONS

    if (Object.keys(attributes).includes(attribute_name)) {
      return attributes[attribute_name].label
    }
    return actions[attribute_name].label
  }

  static getAttributeFromAction(action_name) {
    if (!HTPHelpers.isAttributeAction(action_name)) {
      return action_name
    }
    const attributes = CONFIG.HTP.ATTRIBUTES
    for (const attribute of Object.keys(attributes)) {
      if (attributes[attribute].actions.includes(action_name)) {
        return attribute
      }
    }
  }

  /**
   * Returns true if the attribute is an action
   *
   * @param {string} attribute_name
   * @returns {Boolean}
   */
  static isAttributeAction(attribute_name) {
    const attributes = CONFIG.HTP.ATTRIBUTES

    return !(attribute_name in attributes)
  }

  /* -------------------------------------------- */

  /**
   * Creates options for faction clocks.
   *
   * @param {int[]} sizes
   *  array of possible clock sizes
   * @param {int} default_size
   *  default clock size
   * @param {int} current_size
   *  current clock size
   * @returns {string}
   *  html-formatted option string
   */
  static createListOfClockSizes(sizes, default_size, current_size) {
    let text = ``

    sizes.forEach((size) => {
      text += `<option value="${size}"`
      if (!current_size && size === default_size) {
        text += ` selected`
      } else if (size === current_size) {
        text += ` selected`
      }

      text += `>${size}</option>`
    })

    return text
  }

  static fromUuid(uuid) {
    let parts = uuid.split('.')
    let doc

    const [docName, docId] = parts.slice(0, 2)
    parts = parts.slice(2)
    const collection = CONFIG[docName].collection.instance
    doc = collection.get(docId)

    // Embedded Documents
    while (doc && parts.length > 1) {
      const [embeddedName, embeddedId] = parts.slice(0, 2)
      doc = doc.getEmbeddedDocument(embeddedName, embeddedId)
      parts = parts.slice(2)
    }
    return doc || null
  }
}
