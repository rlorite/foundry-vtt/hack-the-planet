import { AbstractSheet } from './abstract-sheet.js'

export class HTPFactionSheet extends AbstractSheet {
  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ['hack-the-planet', 'sheet', 'actor', 'faction'],
      template: 'systems/hack-the-planet/templates/sheets/faction-sheet.hbs',
      width: 900,
      height: 'auto',
      tabs: [{ navSelector: '.tabs', contentSelector: '.tab-content' }],
    })
  }

  activateListeners(html) {
    super.activateListeners(html)
    html.find("[data-utype='status']").on('click', this._onStatusChange.bind(this))
    html.find('.hold-track').on('change', 'input', this._onHoldChange.bind(this))
  }

  async _onDropItemCreate(itemData) {
    if (itemData.type === 'faction') {
      return super._onDropItemCreate(itemData)
    }
  }

  async _onStatusChange(event) {
    event.preventDefault()
    const faction_id = event.currentTarget.dataset.faction
    const status = parseInt(event.currentTarget.value)
    await this.actor.updateEmbeddedDocuments('Item', [{ _id: faction_id, system: { status } }])
  }

  async _onHoldChange(event) {
    event.preventDefault()
    const faction_id = event.currentTarget.name
    const hold = event.currentTarget.value
    await this.actor.updateEmbeddedDocuments('Item', [{ _id: faction_id, system: { hold } }])
  }
}
