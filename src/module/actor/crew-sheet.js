/**
 * @extends {ActorSheet}
 */
import { AbstractSheet } from './abstract-sheet.js'

export class HTPCrewSheet extends AbstractSheet {
  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ['hack-the-planet', 'sheet', 'actor', 'crew'],
      template: 'systems/hack-the-planet/templates/sheets/crew-sheet.hbs',
      width: 1250,
      height: 800,
      tabs: [{ navSelector: '.tabs', contentSelector: '.tab-content', initial: 'abilities' }],
    })
  }

  /* -------------------------------------------- */

  /** @override */
  getData() {
    const context = {
      ...super.getData(),
    }

    context.experience = [
      game.i18n.localize('HTP.CrewExpClue1'),
      game.i18n.localize('HTP.CrewExpClue2'),
      game.i18n.localize('HTP.CrewExpClue3'),
    ]

    context.crew_type = this.actor.crew_type
    if (context.crew_type) {
      context.experience.splice(1, 0, context.crew_type.system.experience_clues)
    }

    context.friends = [...this.actor.items].filter((item) => item.type === 'friend')
    context.cohorts = [...this.actor.items].filter((item) => item.type === 'cohort')
    context.cohort_harms = CONFIG.HTP.COHORT.harms
    context.vehicles = [...this.actor.items].filter((item) => item.type === 'vehicle')
    context.upgrades = [...this.actor.items].filter((item) => item.type === 'crew_upgrade')
    return context
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html)

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return

    html.find('.resource').on('click', this._onResourceClick.bind(this))
    html.find('.hunting-grounds').on('click', 'span', this._onHuntingGroundsClick.bind(this))
    html.find('.cohort-harm-track').on('change', 'input', this._onCohortHarm.bind(this))
    html.find('.upgrade .level').click(this._onUpgradeLevel.bind(this))
  }

  async _onCohortHarm(event) {
    const itemId = event.currentTarget.closest('.cohort-harm-track').id
    const harm = event.currentTarget.value
    const cohort = this.actor.items.get(itemId)
    return await cohort.update({
      'system.harm': harm,
    })
  }

  /* -------------------------------------------- */
  /*  Form Submission                             */

  /* -------------------------------------------- */

  /** @override */
  async _updateObject(event, formData) {
    // Update the Item
    await super._updateObject(event, formData)

    if (event.target && event.target.name === 'data.tier') {
      this.render(true)
    }
  }

  /* -------------------------------------------- */

  async _onUpgradeLevel(event) {
    event.stopPropagation()
    const itemId = event.currentTarget.closest('.item').dataset.itemId
    const purchaseLevel = event.currentTarget.dataset.level

    await this.actor.updateEmbeddedDocuments('Item', [
      {
        _id: itemId,
        'system.purchased': purchaseLevel,
      },
    ])
  }

  async _onHuntingGroundsClick(event) {
    event.stopPropagation()
    const hunting_ground = event.currentTarget.dataset.huntingGrounds
    await this.actor.update({
      'system.huntingGround': hunting_ground,
    })
  }

  async _onResourceClick(event) {
    event.preventDefault()
    const resource_id = parseInt(event.currentTarget.dataset.resourceId)
    let is_turf = 0
    const resources = this.actor.crew_type.system.resources.map((resource) => {
      if (resource.position === resource_id) {
        resource.acquired = !resource.acquired
        if (resource.turf) is_turf = resource.acquired ? 1 : -1
      }
      return resource
    })
    await this.actor.crew_type.update({
      'data.resources': resources,
    })
    if (is_turf !== 0) {
      const turfs = parseInt(this.actor.system.reputation.turfs)
      await this.actor.update({
        'system.reputation.turfs': turfs + is_turf,
      })
    }
    this.render(true)
  }

  async _onDropItemCreate(itemData) {
    switch (itemData.type) {
      case 'crew_type':
        if (this.actor.crew_type) {
          await this.actor.deleteEmbeddedDocuments('Item', [this.actor.crew_type?.id])
        }
        break
      case 'crew_upgrade':
        itemData.system.purchased = 1
        this._tabs[0].activate('upgrades')
        break
    }
    return await super._onDropItemCreate(itemData)
  }
}
