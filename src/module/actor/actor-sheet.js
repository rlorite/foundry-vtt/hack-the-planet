import { AbstractSheet } from './abstract-sheet.js'
import { HTPActiveEffect } from '../active-effect/index.js'

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {AbstractSheet}
 * @property {HTPActor} actor
 */

export class HTPActorSheet extends AbstractSheet {
  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ['hack-the-planet', 'sheet', 'actor', 'pc'],
      template: 'systems/hack-the-planet/templates/sheets/actor-sheet.hbs',
      width: 1200,
      height: 750,
      tabs: [{ navSelector: '.tabs', contentSelector: '.tab-content', initial: 'abilities' }],
      dragdrop: [{ dropSelector: '.actor-sheet__playbook' }],
    })
  }

  /* -------------------------------------------- */

  /** @override */
  async getData(options) {
    const context = {
      ...super.getData(options),
    }

    context.experience = [
      game.i18n.localize('HTP.ExpClue1'),
      game.i18n.localize('HTP.ExpClue2'),
      game.i18n.localize('HTP.ExpClue3'),
    ]

    context.traumaList = CONFIG.HTP.TRAUMA

    if (this.actor.playbook) {
      context.playbook = this.actor.playbook
      context.experience.splice(1, 0, context.playbook.system.experience)
    }

    context.friends = [...this.actor.items].filter((item) => item.type === 'friend')
    context.special_items = [...this.actor.items].filter(
      (item) => item.type === 'item' && item.system.playbook !== '',
    )
    context.generic_items = [...this.actor.items].filter(
      (item) => item.type === 'item' && item.system.playbook === '',
    )

    // Prepare active effects
    context.effects = this.actor.effects

    // Calculate Load
    context.system.loadout = [...context.items].reduce((acc, item, idx) => {
      if (item.type === 'item' && item.system.equipped) {
        return acc + parseInt(item.system.load)
      }
      return acc
    }, 0)

    context.cybernetics_stress = this.actor.cybernetics_stress

    context.load_levels = {}
    for (const level of Object.keys(CONFIG.HTP.LOAD_LEVELS)) {
      context.load_levels[CONFIG.HTP.LOAD_LEVELS[level].max] = CONFIG.HTP.LOAD_LEVELS[level].label
    }

    context.max_loadout = parseInt(context.system.loadLevel)

    return context
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html)

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return

    html.find('.item .load').click(this._onItemLoadout.bind(this))
    html.find('.trauma-list .trauma').click(this._onTraumaClick.bind(this))
    html.find('.remove-crew').on('click', this._onDeleteCrew.bind(this))

    // manage active effects
    html.find('.effect-control').click((ev) => HTPActiveEffect.onManageActiveEffect(ev, this.actor))
  }

  async _onDrop(event) {
    return super._onDrop(event)
  }

  async _onDeleteCrew(event) {
    await this.actor.update({
      'system.crew': '',
    })
  }

  async _onDropActor(event, data) {
    if (!this.actor.isOwner) return false
    const cls = getDocumentClass('Actor')
    const crew = await cls.fromDropData(data)
    if (!crew) return
    return this.actor.system.addCrew(crew)
  }

  async _onDropItemCreate(itemData) {
    switch (itemData.type) {
      case 'playbook':
        if (this.actor.playbook) {
          await this.actor.deleteEmbeddedDocuments('Item', [this.actor.playbook._id])
        }
        break
    }
    return super._onDropItemCreate(itemData)
  }

  async _onTraumaClick(event) {
    const trauma = event.currentTarget.dataset.trauma
    const trauma_list = this.actor.system.trauma.list
    trauma_list[trauma] = !trauma_list[trauma]
    const trauma_value = Object.values(trauma_list).reduce((acc, value) => {
      if (value) return ++acc
      return acc
    }, 0)
    await this.actor.update({
      'system.trauma.list': trauma_list,
      'system.trauma.current': trauma_value,
    })
  }

  _onItemLoadout(event) {
    event.stopPropagation()
    let load = parseInt(event.currentTarget.dataset.load)
    const itemId = event.currentTarget.closest('.item').dataset.itemId
    const item = this.actor.items.get(itemId)
    let equipped = true
    if (load <= 1 && item.system.equipped) {
      equipped = false
      load = 0
    }
    return item.update({
      'system.load': load,
      'system.equipped': equipped,
      'system.max_load': item.system.max_load,
    })
  }

  async createPlaybook(playbook) {
    // return await this.actor.createEmbeddedDocuments('Item', [playbook])
  }
}
