import Clocks from '../clocks.js'
import { HTPHelpers } from '../helpers.js'

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 * @property {HTPActor} actor
 */
export class AbstractSheet extends ActorSheet {
  getData(options) {
    const context = {
      ...super.getData(options),
      editable: this.options.editable,
      isGM: game.user.isGM,
      system: this.actor.system,
    }

    context.experience = []
    let ability_key = 'ability'
    if (this.actor.type === 'crew') {
      ability_key = 'crew_ability'
    }
    context.abilities = [...this.actor.items]
      .filter((item) => item.type === ability_key)
      .sort((a, b) =>
        a.system.starting_ability === b.system.starting_ability
          ? 0
          : a.system.starting_ability
            ? -1
            : 1,
      )

    return context
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html)
    // Update Inventory Item
    html.find('.item').click((ev) => {
      const itemId = $(ev.currentTarget).data('itemId')
      const item = this.actor.items.get(itemId)
      item.sheet.render(true)
    })

    // Delete Inventory Item
    html.find('.item-delete').click(async (ev) => {
      ev.stopPropagation()
      const element = $(ev.currentTarget).parents('.item')
      await this.actor.deleteEmbeddedDocuments('Item', [element.data('itemId')])
      element.slideUp(200, () => this.render(false))
    })
    html.find('.ability .purchase').click(this._onAbilityPurchased.bind(this))
    html.find('.item-add-popup').click(this._onItemAddClick.bind(this))
    html.find('.friends .disposition').click(this._onFriendDisposition.bind(this))
    // This is a workaround until is being fixed in FoundryVTT.
    if (this.options.submitOnChange) {
      html.on('change', 'textarea', this._onChangeInput.bind(this)) // Use delegated listener on the form
    }

    html.find('.roll-die-attribute').click(this._onRollAttributeDieClick.bind(this))
    // html.find('.roll-die-fortune').click(this._onRollFortuneClick.bind(this))
    html.find('.cohort-roll').click(this._onRollCohortClick.bind(this))
    html.find('.add-clock').on('click', this._onAddClockClick.bind(this))
    html.find('.remove-clock').on('click', this._onDeleteClock.bind(this))
    Clocks.bind(html, this.actor)
  }

  /* -------------------------------------------- */

  _onAbilityPurchased(event) {
    event.stopPropagation()
    const itemId = event.currentTarget.closest('.ability').dataset.itemId
    const ability = this.actor.items.get(itemId)
    const purchased = !ability.system.purchased
    return ability.update({
      'system.purchased': purchased,
    })
  }

  async _onItemAddClick(event) {
    event.preventDefault()
    const item_type = $(event.currentTarget).data('itemType')
    const distinct = $(event.currentTarget).data('distinct')
    let input_type = 'checkbox'

    if (typeof distinct !== 'undefined') {
      input_type = 'radio'
    }

    const items = await HTPHelpers.getAllItemsByType(item_type, game)

    let html = `<div class="items-to-add">`

    items.forEach((e) => {
      let addition_price_load = ``

      if (typeof e.data.load !== 'undefined') {
        addition_price_load += `(${e.data.load})`
      }

      html += `<input id="select-item-${e._id}" type="${input_type}" name="select_items" value="${e._id}">`
      html += `<label class="flex-horizontal" for="select-item-${e._id}">`
      html += `${game.i18n.localize(e.name)} ${addition_price_load} <i class="tooltip fas fa-question-circle"><span class="tooltiptext">${game.i18n.localize(e.data.description)}</span></i>`
      html += `</label>`
    })

    html += `</div>`

    const options = {
      // width: "500"
    }

    const dialog = new Dialog(
      {
        title: `${game.i18n.localize('Add')} ${item_type}`,
        content: html,
        buttons: {
          one: {
            icon: '<i class="fas fa-check"></i>',
            label: game.i18n.localize('Add'),
            callback: async (html) =>
              await this.addItemsToSheet(item_type, $(html).find('.items-to-add')),
          },
          two: {
            icon: '<i class="fas fa-times"></i>',
            label: game.i18n.localize('Cancel'),
            callback: () => false,
          },
        },
        default: 'two',
      },
      options,
    )

    dialog.render(true)
  }

  /* -------------------------------------------- */

  async addItemsToSheet(item_type, el) {
    const items = await HTPHelpers.getAllItemsByType(item_type, game)
    const items_to_add = []

    el.find('input:checked').each(function () {
      items_to_add.push(items.find((e) => e._id === $(this).val()))
    })

    await Item.create(items_to_add, { parent: this.document })
  }

  /* -------------------------------------------- */

  /**
   * Roll an Attribute die.
   * @param {*} event
   */
  async _onRollAttributeDieClick(event) {
    const attribute_name = $(event.currentTarget).data('rollAttribute')
    this.actor.rollAttributePopup(attribute_name)
  }

  async _onRollCohortClick(event) {
    const cohort = this.actor.items.get(event.currentTarget.dataset.cohort)

    await this.actor.rollAttributePopup('cohort', {
      cohort,
    })
  }

  _onFriendDisposition(event) {
    event.stopPropagation()
    let disposition = event.currentTarget.classList.contains('item__friendly')
      ? 'friendly'
      : 'enemy'
    const itemId = event.currentTarget.closest('.item').dataset.itemId
    const friend = this.actor.items.get(itemId)
    if (friend.system.disposition === disposition) {
      disposition = ''
    }
    return friend.update({
      'system.disposition': disposition,
    })
  }

  async _onDeleteClock(event) {
    const clocks = this.actor.system.clocks
    const clockId = event.currentTarget.dataset.itemId
    clocks[clockId].deleted = true

    await this.actor.update({
      'system.clocks': clocks,
    })
  }

  _onAddClockClick(event) {
    const clock_sizes = HTPHelpers.createListOfClockSizes(CONFIG.HTP.CLOCKS.sizes, 4, 4)
    const clocks = this.actor.system.clocks
    const dialog = new Dialog({
      title: game.i18n.localize('HTP.AddClock'),
      content: `<div class="clock-form">
                <label class="flex-horizontal" for="clock-text">
                    <span>Texto del reloj</span>
                    <input id="clock-text" type="text" required name="clock-text" value="">
                </label>
                <label class="flex-horizontal" for="clock-segments">
                    <span>Sectores</span>
                    <select name="clock-segments" id="clock-segments">
                        ${clock_sizes}
                    </select>
                </label></div>`,
      buttons: {
        ok: {
          icon: '<i class="fas fa-check"></i>',
          label: game.i18n.localize('HTP.AddClock'),
          callback: async (html) => {
            const clock_text = html.find("[name='clock-text']").val()
            const clock_segments = html.find("[name='clock-segments']").val()
            clocks.push({ title: clock_text, segments: clock_segments, value: 0 })

            await this.actor.update({
              'system.clocks': clocks,
            })
          },
        },
        cancel: {
          icon: '<i class="fas fa-times"></i>',
          label: game.i18n.localize('Cancel'),
          callback: () => false,
        },
      },
      default: 'cancel',
    })

    dialog.render(true)
  }
}
