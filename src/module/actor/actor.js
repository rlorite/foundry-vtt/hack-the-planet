import HTPRoll from '../roll.js'
import { HTPHelpers } from '../helpers.js'

export const PLAYBOOK_ITEM_TYPES = ['ability', 'item', 'friend']
export const CREW_ITEM_TYPES = ['crew_ability']

export class HTPActor extends Actor {
  applyActiveEffects() {
    super.applyActiveEffects()
    this.system.members?.forEach((actor) => {
      actor.reset()
      actor.render()
    })
  }

  *allApplicableEffects() {
    yield* super.allApplicableEffects()
    if (this.type === 'character' && this.system.crew) {
      for (const effect of this.system.crew.allApplicableEffects()) {
        if (effect.isApplicableTo(this.type)) {
          effect.flags['hack-the-planet'].fromCharacter = true
          yield effect
        }
      }
    }
  }

  /** @override */
  static async create(data, options = {}) {
    data.token = data.token || {}

    // For Crew and Character set the Token to sync with charsheet.
    switch (data.type) {
      case 'character':
      case 'crew':
      case '\uD83D\uDD5B clock':
        data.token.actorLink = true
        break
      case 'factions':
        data.img = 'systems/hack-the-planet/assets/icons/factions.svg'
    }

    const actor = await super.create(data, options)
    if (data.type === 'character') {
      /** @type {HTPItem[]} **/
      const items = await HTPHelpers.getAllItemsByType('item')

      for (const item of items) {
        if (item.system.playbook === '') {
          await Item.create(item.toObject(), { parent: actor })
        }
      }
    }
    return actor
  }

  /** @override */
  getRollData() {
    const data = super.getRollData()

    data.dice_amount = this.getAttributeDiceToThrow()

    return data
  }

  _onUpdate(changed, options, userId) {
    super._onUpdate(changed, options, userId)
    this.system._onUpdate?.(changed, options, userId)
  }

  static async update(data, context) {
    await self.update(data, context)
  }

  _onCreateDescendantDocuments(parent, collection, documents, data, options, userId) {
    super._onCreateDescendantDocuments(parent, collection, documents, data, options, userId)
    for (const document of documents) {
      switch (document.type) {
        case 'vehicle':
          return this._updateFuel()
        case 'playbook':
          return this._onCreatePlaybook(document)
        case 'crew_type':
          return this._onCreateCrewType(document)
      }
    }
  }

  async _onDeleteDescendantDocuments(parent, collection, documents, ...args) {
    for (const document of documents) {
      switch (document.type) {
        case 'playbook':
          await this._onDeletePlaybook(document)
          break
        case 'vehicle':
          await this._updateFuel()
          break
        case 'crew_type':
          await this._onDeleteCrewType(document)
          break
      }
    }
    super._onDeleteDescendantDocuments(parent, collection, documents, ...args)
  }

  async _updateFuel() {
    const useFuel = this.items.some((item) => item.type === 'vehicle' && item.system.fuel)
    await this.update({
      'system.fuel.disabled': !useFuel,
    })
  }

  /**
   * Calculate Attribute Dice to throw.
   */
  getAttributeDiceToThrow() {
    // Calculate Dice to throw.
    const dice_amount = {}
    for (const attribute_name in this.system.attributes) {
      dice_amount[attribute_name] = 0
      const attribute = this.system.attributes[attribute_name]
      for (const [action_name, action] of Object.entries(this.system.actions)) {
        if (attribute.actions.includes(action_name)) {
          let actionValue = parseInt(action.value)
          if (actionValue < action.initial) {
            actionValue = action.initial
          }
          dice_amount[action_name] = actionValue
          if (action.cyberware) {
            dice_amount[action_name]++
          }

          // We add a +1d for every skill higher than 0.
          if (dice_amount[action_name] > 0) {
            dice_amount[attribute_name]++
          } else if (action.cyberware) {
            dice_amount[attribute_name]++
          }
        }
      }
    }
    return dice_amount
  }

  async rollAttributePopup(attribute_name, options = {}) {
    let content, title, callback
    switch (attribute_name) {
      case 'cohort':
        title = game.i18n.localize('HTP.Rolls.FortuneRoll.Title')
        content = await renderTemplate('systems/hack-the-planet/templates/rolls/cohort_popup.hbs', {
          title: options.cohort.name,
          modifiers: {
            options: this.createListOfDiceMods(0, +5),
            selected: 0,
          },
        })
        callback = async (html) => {
          const cohort_apply = html.find('[name="type_apply"]')[0].checked
          let dices = parseInt(html.find('[name="mod"]')[0].value)
          if (cohort_apply) {
            dices += parseInt(options.cohort.system.quality)
            if (options.cohort.system.elite) {
              dices++
            }
          }
          await new HTPRoll(dices).fortuneRoll()
        }
        break
      case 'heal':
        title = game.i18n.localize('HTP.HealingRoll')
        content = await renderTemplate(
          'systems/hack-the-planet/templates/rolls/fortune_popup.hbs',
          {
            title,
            modifiers: {
              options: this.createListOfDiceMods(0, +5),
              selected: 0,
            },
          },
        )
        callback = async (html) => {
          const dices = parseInt(html.find('[name="mod"]')[0].value)
          await new HTPRoll(dices).healingRoll(this)
        }
        break
      case 'vice':
        title = game.i18n.localize('HTP.Rolls.ViceRoll.Title')
        content = await renderTemplate('systems/hack-the-planet/templates/rolls/vice_popup.hbs', {
          title,
        })
        callback = async (html) => {
          const rollData = this.getAttributeDiceToThrow()
          await new HTPRoll(
            Math.min(rollData.insight, rollData.prowess, rollData.resolve),
          ).viceRoll(this)
        }

        break
      default: {
        const attribute_label = HTPHelpers.getAttributeLabel(attribute_name)
        const is_attribute_action = HTPHelpers.isAttributeAction(attribute_name)
        title = `${is_attribute_action ? game.i18n.localize('HTP.Rolls.ActionRoll') : game.i18n.localize('HTP.Rolls.ResistanceRoll')}`
        content = await renderTemplate('systems/hack-the-planet/templates/rolls/attribute.hbs', {
          is_attribute_action,
          attribute_label,
          modifiers: {
            options: this.createListOfDiceMods(-3, +3),
            selected: 0,
          },
        })
        callback = async (html) => {
          const modifier = parseInt(html.find('[name="mod"]')[0].value)
          if (is_attribute_action) {
            const position = html.find('[name="pos"]')[0].value
            const effect = html.find('[name="fx"]')[0].value
            await this.rollAttribute(attribute_name, modifier, position, effect)
          } else {
            await this.rollResistance(attribute_name, modifier)
          }
        }
      }
    }

    new Dialog({
      title,
      content,
      buttons: {
        yes: {
          icon: "<i class='fas fa-check'></i>",
          label: game.i18n.localize('HTP.Rolls.Roll'),
          callback,
        },
        no: {
          icon: "<i class='fas fa-times'></i>",
          label: game.i18n.localize('Close'),
        },
      },
      default: 'yes',
    }).render(true)
  }

  /* -------------------------------------------- */

  dice_ammount(attribute_name, additional_dice_amount) {
    let dice_amount = 0
    if (attribute_name !== '') {
      const roll_data = this.getRollData()
      dice_amount += roll_data.dice_amount[attribute_name]
    } else {
      dice_amount = 1
    }
    dice_amount += additional_dice_amount
    return dice_amount
  }

  async rollAttribute(attribute_name = '', additional_dice_amount = 0, position, effect) {
    const dice_amount = this.dice_ammount(attribute_name, additional_dice_amount)
    await new HTPRoll(dice_amount, attribute_name, position, effect).attributeRoll(this)
  }

  async rollResistance(attribute_name, additional_dice_amount) {
    const dice_amount = this.dice_ammount(attribute_name, additional_dice_amount)
    await new HTPRoll(dice_amount, attribute_name).resistanceRoll(this)
  }

  /* -------------------------------------------- */

  /**
   * Create <options> for available actions
   *  which can be performed.
   */
  createListOfActions() {
    let text, attribute, skill
    const attributes = this.system.attributes

    for (attribute in attributes) {
      const skills = attributes[attribute].skills

      text += `<optgroup label="${attribute} Actions">`
      text += `<option value="${attribute}">${attribute} (Resist)</option>`

      for (skill in skills) {
        text += `<option value="${skill}">${skill}</option>`
      }

      text += `</optgroup>`
    }

    return text
  }

  /* -------------------------------------------- */

  /**
   * Creates <options> modifiers for dice roll.
   *
   * @param {int} rs
   *  Min die modifier
   * @param {int} re
   *  Max die modifier
   * @param {int} s
   *  Selected die
   */
  createListOfDiceMods(rs, re) {
    const options = []

    for (let i = rs; i <= re; i++) {
      let plus = ''
      if (i >= 0) {
        plus = '+'
      }
      const label = `${plus}${i}d`
      const option = {
        key: i.toString(),
        label,
      }
      options.push(option)
    }

    return options
  }

  async clearStress(stress) {
    const currentStress = parseInt(this.system.stress.current)
    await this.update({
      'system.stress.current': Math.max(0, currentStress - stress),
    })
  }

  async applyStress(stress) {
    const currentStress = parseInt(this.system.stress.current)
    let newStress = currentStress + parseInt(stress)
    let traumas = 0
    const maxStress = this.system.stress.max - this.cybernetics_stress
    if (newStress >= maxStress) {
      traumas = Math.floor(newStress / maxStress)
      newStress = newStress % maxStress
    }
    await this.update({
      'system.stress.current': newStress,
    })
    return traumas
  }

  async applyHealing(segments) {
    const total_segments = parseInt(segments) + parseInt(this.system.healingClock)

    await this.update({
      'system.healingClock': total_segments % 4,
    })
    return Math.floor(total_segments / 4)
  }

  async applyExperience(experience, category) {
    let currentExperience, maxExperience
    const update = {}
    switch (category) {
      case 'insight':
      case 'prowess':
      case 'resolve':
        currentExperience = this.system.attributes[category].exp
        maxExperience = this.system.attributes[category].exp_max
        update[`system.attributes.${category}.exp`] = Math.min(
          currentExperience + experience,
          maxExperience,
        )
        break
      case 'playbook':
      case 'stress':
        currentExperience = this.system.experience[category].current
        maxExperience = this.system.experience[category].max
        update[`system.experience.${category}.current`] = Math.min(
          currentExperience + experience,
          maxExperience,
        )
        break
    }
    return this.update(update)
  }

  get cybernetics_stress() {
    const cyberware_marks = Object.values(this.system.actions).filter(
      (action) => action.cyberware,
    ).length
    if (cyberware_marks >= 4) return 6
    if (cyberware_marks >= 3) return 3
    if (cyberware_marks >= 2) return 1
    return 0
  }

  get crew_type() {
    return this.items.find((item) => item.type === 'crew_type')
  }

  get playbook() {
    return this.items.find((item) => item.type === 'playbook')
  }

  async _onCreatePlaybook(playbook) {
    for (const item_type of PLAYBOOK_ITEM_TYPES) {
      const items = await HTPHelpers.getAllItemsByType(item_type)
      const playbook_items = items.filter((item) => item.system.playbook === playbook.name)
      for (const item of playbook_items) {
        await Item.create(item.toObject(), { parent: this })
      }
    }

    const friends = playbook.system.friends
    for (const friend of friends) {
      friend.data.playbook = playbook.name
      await Item.create(friend, { parent: this })
    }
  }

  async _onDeletePlaybook(playbook) {
    // Remove items associated to playbook
    for (const item of this.items) {
      if (PLAYBOOK_ITEM_TYPES.includes(item.type) && item.system.playbook === playbook.name) {
        await item.delete()
      }
    }
  }

  async _onCreateCrewType(crewType) {
    await this.update({
      img: crewType.img,
    })
    for (const item_type of CREW_ITEM_TYPES) {
      const items = await HTPHelpers.getAllItemsByType(item_type)
      const crew_items = items.filter((item) => item.system.playbook === crewType.name)
      for (const item of crew_items) {
        await Item.create(item, { parent: this })
      }
    }
    const friends = crewType.system.friends
    for (const friend of friends) {
      await Item.create(friend, { parent: this })
    }
    for (const upgrade of crewType.system.starting_upgrades) {
      foundry.utils.mergeObject(upgrade, {
        system: {
          starting_upgrade: true,
          purchased: 1,
        },
      })
      await Item.create(upgrade, { parent: this })
    }
  }

  async _onDeleteCrewType(crewType) {
    for (const item of this.items) {
      if (CREW_ITEM_TYPES.includes(item.type) && item.system.playbook === crewType.name) {
        await item.delete()
      }
    }
  }
}
