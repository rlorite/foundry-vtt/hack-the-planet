const HTP = {}
HTP.MAX_STRESS = 12
HTP.TRAUMA = {
  haunted: 'HTP.TraumaHaunted',
  obsessed: 'HTP.TraumaObsessed',
  paranoid: 'HTP.TraumaParanoid',
  reckless: 'HTP.TraumaReckless',
  soft: 'HTP.TraumaSoft',
  unstable: 'HTP.TraumaUnstable',
  vicious: 'HTP.TraumaVicious',
}
HTP.ATTRIBUTES = {
  insight: {
    label: 'HTP.SkillsInsight',
    actions: ['trace', 'study', 'survey', 'modify'],
  },
  prowess: {
    label: 'HTP.SkillsProwess',
    actions: ['finesse', 'ghost', 'combat', 'skirmish'],
  },
  resolve: {
    label: 'HTP.SkillsResolve',
    actions: ['control', 'sway', 'hack', 'network'],
  },
}
HTP.ACTIONS = {
  trace: 'HTP.SkillsTrace',
  study: 'HTP.SkillsStudy',
  survey: 'HTP.SkillsSurvey',
  modify: 'HTP.SkillsModify',
  finesse: 'HTP.SkillsFinesse',
  ghost: 'HTP.SkillsGhost',
  combat: 'HTP.SkillsCombat',
  skirmish: 'HTP.SkillsSkirmish',
  control: 'HTP.SkillsControl',
  sway: 'HTP.SkillsSway',
  hack: 'HTP.SkillsHack',
  network: 'HTP.SkillsNetwork',
}

HTP.LOAD_LEVELS = {
  LIGHT: {
    label: 'HTP.LoadLight',
    max: 3,
  },
  NORMAL: {
    label: 'HTP.LoadNormal',
    max: 5,
  },
  HEAVY: {
    label: 'HTP.LoadHeavy',
    max: 6,
  },
  ENCUMBERED: {
    label: 'HTP.LoadEncumbered',
    max: 9,
  },
}

HTP.CLOCKS = {
  sizes: [2, 4, 6, 8, 10, 12, 14],
}

HTP.LEVELABLE_ITEM_TYPES = ['crew_upgrade']
HTP.TRANSFERABLE_ITEM_TYPES = ['crew_upgrade']

HTP.COHORT = {
  types: {
    Gang: 'HTP.Cohorts.Types.Gang',
    Expert: 'HTP.Cohorts.Types.Expert',
  },
  gangs: {
    Boosters: {
      label: 'HTP.Cohorts.Gangs.Types.Boosters.Label',
      description: 'HTP.Cohorts.Gangs.Types.Boosters.Description',
    },
    Monitors: {
      label: 'HTP.Cohorts.Gangs.Types.Monitors.Label',
      description: 'HTP.Cohorts.Gangs.Types.Monitors.Description',
    },
    Rooks: {
      label: 'HTP.Cohorts.Gangs.Types.Rooks.Label',
      description: 'HTP.Cohorts.Gangs.Types.Rooks.Description',
    },
    Rovers: {
      label: 'HTP.Cohorts.Gangs.Types.Rovers.Label',
      description: 'HTP.Cohorts.Gangs.Types.Rovers.Description',
    },
    Runners: {
      label: 'HTP.Cohorts.Gangs.Types.Runners.Label',
      description: 'HTP.Cohorts.Gangs.Types.Runners.Description',
    },
  },
  edge_list: {
    Fearsome: {
      label: 'HTP.CohortEdgeFearsome',
      description: 'HTP.CohortEdgeFearsomeDescription',
    },
    Independent: {
      label: 'HTP.CohortEdgeIndependent',
      description: 'HTP.CohortEdgeIndependentDescription',
    },
    Loyal: {
      label: 'HTP.CohortEdgeLoyal',
      description: 'HTP.CohortEdgeLoyalDescription',
    },
    Tenacious: {
      label: 'HTP.CohortEdgeTenacious',
      description: 'HTP.CohortEdgeTenaciousDescription',
    },
  },
  flaw_list: {
    Principled: {
      label: 'HTP.CohortFlawPrincipled',
      description: 'HTP.CohortFlawPrincipledDescription',
    },
    Savage: {
      label: 'HTP.CohortFlawSavage',
      description: 'HTP.CohortFlawSavageDescription',
    },
    Unreliable: {
      label: 'HTP.CohortFlawUnreliable',
      description: 'HTP.CohortFlawUnreliableDescription',
    },
    Wild: {
      label: 'HTP.CohortFlawWild',
      description: 'HTP.CohortFlawWildDescription',
    },
  },
  harms: {
    NoHarm: {
      label: 'HTP.HarmNoHarm',
      description: 'HTP.HarmNoHarmDescription',
      value: 0,
    },
    Weakened: {
      label: 'HTP.HarmWeakened',
      description: 'HTP.HarmWeakenedDescription',
      value: 1,
    },
    Impaired: {
      label: 'HTP.HarmImpaired',
      description: 'HTP.HarmImpairedDescription',
      value: 2,
    },
    Broken: {
      label: 'HTP.HarmBroken',
      description: 'HTP.HarmBrokenDescription',
      value: 3,
    },
    Dead: {
      label: 'HTP.HarmDead',
      description: 'HTP.HarmDeadDescription',
      value: 4,
    },
  },
}

HTP.VEHICLES = {
  edge_list: {
    Nimble: {
      label: 'HTP.VehicleEdgeNimble',
      description: 'HTP.VehicleEdgeNimbleDescription',
    },
    Simple: {
      label: 'HTP.VehicleEdgeSimple',
      description: 'HTP.VehicleEdgeSimpleDescription',
    },
    Sturdy: {
      label: 'HTP.VehicleEdgeSturdy',
      description: 'HTP.VehicleEdgeSturdyDescription',
    },
    Equipped: {
      label: 'HTP.VehicleEdgeEquipped',
      description: 'HTP.VehicleEdgeEquippedDescription',
    },
  },
  flaw_list: {
    Costly: {
      label: 'HTP.VehicleFlawCostly',
      description: 'HTP.VehicleFlawCostlyDescription',
    },
    Distinct: {
      label: 'HTP.VehicleFlawDistinct',
      description: 'HTP.VehicleFlawDistinctDescription',
    },
    Illegal: {
      label: 'HTP.VehicleFlawIllegal',
      description: 'HTP.VehicleFlawIllegalDescription',
    },
    Finicky: {
      label: 'HTP.VehicleFlawFinicky',
      description: 'HTP.VehicleFlawFinickyDescription',
    },
  },
  harms: HTP.COHORT.harms,
}

HTP.RESOURCE = {
  connections: {
    top: 'HTP.ResourceConnectionTop',
    topRight: 'HTP.ResourceConnectionTopRight',
    bottomRight: 'HTP.ResourceConnectionBottomRight',
    bottom: 'HTP.ResourceConnectionBottom',
    bottomLeft: 'HTP.ResourceConnectionBottomLeft',
    topLeft: 'HTP.ResourceConnectionTopLeft',
  },
}
export default HTP
