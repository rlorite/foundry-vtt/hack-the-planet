export class ChatCard {
  /**
   *
   * @param {ChatMessage} message
   * @param {jQuery} html
   */
  constructor(message, html) {
    this.updateBinding(message, html)
  }

  /**
   *
   * @returns {ChatMessage | undefined}
   */
  get message() {
    return game.messages?.get(this.id || '')
  }

  /**
   *
   * @param {ChatMessage} message
   * @param {jQuery} html
   */
  updateBinding(message, html) {
    this.id = message.id
    html.find('.stress').on('click', this._markStress.bind(this))
    html.find('.vice').on('click', this._clearStress.bind(this))
    html.find('.heal').on('click', this._markHeal.bind(this))
    html.find('.xp').on('click', this._markExperience.bind(this))
  }

  async _markStress(event) {
    event.preventDefault()

    const { actor, stress } = event.currentTarget.dataset
    const theActor = game.actors?.get(actor)
    const traumas = await theActor.applyStress(stress)
    const html = $(event.currentTarget).parents('.message-content')
    html.find('.mark-stress').html(`
            <div class="chat-info ${traumas ? 'warning' : 'success'}">
                <span>${game.i18n.localize('HTP.StressApplied')}</span>
                <span>${traumas ? game.i18n.format('HTP.StressMarkTraumas', { traumas }) : ''}</span>
            </div>
        `)
    await this.message?.update({
      content: html.html(),
    })
  }

  async _clearStress(event) {
    event.preventDefault()

    const { actor, stress } = event.currentTarget.dataset
    const theActor = game.actors?.get(actor)
    await theActor.clearStress(stress)
    const html = $(event.currentTarget).parents('.message-content')
    html.find('.mark-stress').html(`
            <div class="chat-info success">
                <span>${game.i18n.localize('HTP.Rolls.ViceRoll.StressCleared')}</span>
            </div>
        `)
    await this.message?.update({
      content: html.html(),
    })
  }

  async _markHeal(event) {
    event.preventDefault()

    const { actor, segments } = event.currentTarget.dataset
    const theActor = game.actors?.get(actor)
    const healing_levels = await theActor.applyHealing(segments)

    const html = $(event.currentTarget).parents('.message-content')
    html.find('.mark-heal').html(`
            <div class="chat-info ${healing_levels ? 'warning' : 'success'}">
                <span>${game.i18n.localize('HTP.HealingApplied')}</span>
                <span>${healing_levels ? game.i18n.format('HTP.HealingLevels', { levels: healing_levels }) : ''}</span>
            </div>
        `)
    await this.message?.update({
      content: html.html(),
    })
  }

  async _markExperience(event) {
    event.preventDefault()
    const { actor, experience, category } = event.currentTarget.dataset
    await game.actors?.get(actor).applyExperience(Number(experience), category)
    const html = $(event.currentTarget).parents('.message-content')

    html.find('button.xp').replaceWith(`
            <div class="chat-info success">
                <span><i class="fas fa-check"></i> ${game.i18n.localize('HTP.Experience.Marked')}</span>
            </div>
        `)
    await this.message?.update({
      content: html.html(),
    })
  }

  static async bind(message, html) {
    const chatCard = message.chatCard
    if (chatCard) {
      chatCard.updateBinding(message, html)
    } else {
      message.chatCard = new ChatCard(message, html)
    }
  }

  static registerHooks() {
    Hooks.on('renderChatMessage', ChatCard.bind)
  }
}
