/**
 * Roll Dice.
 * @param {int} dice_amount
 * @param {string} attribute_name
 * @param {string} position
 * @param {string} effect
 */
import { HTPHelpers } from './helpers.js'

export default class HTPRoll extends Roll {
  constructor(dice_amount, attribute_name = '', position = 'risky', effect = 'standard') {
    dice_amount = Number(dice_amount)
    let zeromode = false

    if (dice_amount < 0) {
      dice_amount = 0
    }
    if (dice_amount === 0) {
      zeromode = true
      dice_amount = 2
    }
    super(`${dice_amount}d6`)
    this.zeromode = zeromode
    this.attribute_name = attribute_name
    this.position = position
    this.effect = effect
  }

  get rolls() {
    return this.terms[0].results
  }

  getRollStatus() {
    const sorted_rolls = this.rolls.map((i) => i.result).sort()

    let roll_status = 'failure'

    if (sorted_rolls[0] === 6 && this.zeromode) {
      roll_status = 'critical-success'
    } else {
      let use_die
      let prev_use_die = false

      if (this.zeromode) {
        use_die = sorted_rolls[0]
      } else {
        use_die = sorted_rolls[sorted_rolls.length - 1]

        if (sorted_rolls.length - 2 >= 0) {
          prev_use_die = sorted_rolls[sorted_rolls.length - 2]
        }
      }

      // 1,2,3 = failure
      if (use_die <= 3) {
        roll_status = 'failure'
      }
      // if 6 - check the prev highest one.
      else if (use_die === 6) {
        // 6,6 - critical success
        if (prev_use_die && prev_use_die === 6) {
          roll_status = 'critical-success'
        }
        // 6 - success
        else {
          roll_status = 'success'
        }
      }
      // else (4,5) = partial success
      else {
        roll_status = 'partial-success'
      }
    }

    return roll_status
  }

  getRollStress() {
    let stress = 6

    // Sort roll values from lowest to highest.
    const sorted_rolls = this.rolls.map((i) => i.result).sort()

    if (sorted_rolls[0] === 6 && this.zeromode) {
      stress = -1
    } else {
      let use_die
      let prev_use_die = false

      if (this.zeromode) {
        use_die = sorted_rolls[0]
      } else {
        use_die = sorted_rolls[sorted_rolls.length - 1]

        if (sorted_rolls.length - 2 >= 0) {
          prev_use_die = sorted_rolls[sorted_rolls.length - 2]
        }
      }

      if (use_die === 6 && prev_use_die && prev_use_die === 6) {
        stress = -1
      } else {
        stress = 6 - use_die
      }
    }

    return stress
  }

  async viceRoll(actor) {
    if (!this._evaluated) await this.evaluate({ async: true })

    const highest_roll = this.rolls.map((i) => i.result).sort()[0]
    const messageData = {
      content: await renderTemplate('systems/hack-the-planet/templates/rolls/vice.hbs', {
        rolls: this.rolls,
        stress: highest_roll,
        actor,
        abuse: highest_roll > actor.system.stress.current,
      }),
    }

    return this.toMessage(messageData)
  }

  async healingRoll(actor) {
    if (!this._evaluated) await this.evaluate({ async: true })
    const sorted_rolls = this.rolls.map((i) => i.result).sort()
    let segments = 0
    if (sorted_rolls[0] === 6 && this.zeromode) {
      segments += 5
      return
    }
    if (this.zeromode) {
      sorted_rolls.pop()
    }
    sorted_rolls.forEach((roll, idx) => {
      if (roll === 6) {
        segments += 3
        return
      }
      if (roll >= 4) {
        segments += 2
        return
      }
      segments++
    })
    const messageData = {
      content: await renderTemplate('systems/hack-the-planet/templates/rolls/heal.hbs', {
        rolls: this.rolls,
        segments,
        actor,
      }),
    }
    return this.toMessage(messageData)
  }

  async resistanceRoll(actor) {
    if (!this._evaluated) await this.evaluate({ async: true })

    const stress = this.getRollStress()
    const messageData = {
      content: await renderTemplate('systems/hack-the-planet/templates/rolls/resistance.hbs', {
        rolls: this.rolls,
        stress,
        actor,
      }),
    }

    return this.toMessage(messageData)
  }

  async fortuneRoll() {
    if (!this._evaluated) await this.evaluate({ async: true })
    const roll_status = this.getRollStatus()

    const messageData = {
      content: await renderTemplate('systems/hack-the-planet/templates/rolls/fortune.hbs', {
        rolls: this.rolls,
        roll_status,
        attribute_label: this.attribute_name,
      }),
    }
    return this.toMessage(messageData)
  }

  async engagementRoll() {
    if (!this._evaluated) await this.evaluate({ async: true })

    return this.toMessage({
      content: await renderTemplate('systems/hack-the-planet/templates/rolls/engagement.hbs', {
        rolls: this.rolls,
        roll_status: this.getRollStatus(),
      }),
    })
  }

  async attributeRoll(actor) {
    if (!this._evaluated) await this.evaluate({ async: true })

    const attribute_label = HTPHelpers.getAttributeLabel(this.attribute_name)
    // Retrieve Roll status.
    const roll_status = this.getRollStatus()

    const roll_data = {
      rolls: this.rolls,
      roll_status,
      attribute_label,
      attribute: HTPHelpers.getAttributeFromAction(this.attribute_name),
    }
    let position_localize
    switch (this.position) {
      case 'controlled':
        position_localize = 'HTP.PositionControlled'
        break
      case 'desperate':
        position_localize = 'HTP.PositionDesperate'
        break
      case 'risky':
      default:
        position_localize = 'HTP.PositionRisky'
    }
    let effect_localize
    switch (this.effect) {
      case 'limited':
        effect_localize = 'HTP.EffectLimited'
        break
      case 'great':
        effect_localize = 'HTP.EffectStandard'
        break
      case 'standard':
      default:
        effect_localize = 'HTP.EffectStandard'
    }
    const messageData = {
      content: await renderTemplate('systems/hack-the-planet/templates/rolls/action.hbs', {
        ...roll_data,
        position: this.position,
        position_localize,
        effect: this.effect,
        effect_localize,
        actor,
      }),
    }
    return this.toMessage(messageData)
  }
}

/**
 * Call a Roll popup.
 */
export async function simpleRollPopup() {
  const dialog = new Dialog(
    {
      title: game.i18n.localize('HTP.Rolls.HTPRoll'),
      content: await renderTemplate('systems/hack-the-planet/templates/dialogs/dice-roller.hbs', {
        dices: Array(11)
          .fill('')
          .map((item, i) => `<option value="${i}">${i}D</option>`)
          .join(''),
      }),
      buttons: {
        yes: {
          icon: "<i class='fas fa-check'></i>",
          label: game.i18n.localize('HTP.Rolls.Roll'),
          callback: async (html) => {
            const roll_type = html.find('[name="roll-type"]').val()
            const dice_number = html.find('[name="dice-number"]').val()
            const roll = new HTPRoll(dice_number)
            switch (roll_type) {
              case 'fortune':
                return roll.fortuneRoll()
              case 'engagement':
                return roll.engagementRoll()
            }
          },
        },
        no: {
          icon: "<i class='fas fa-times'></i>",
          label: game.i18n.localize('Cancel'),
        },
      },
      default: 'yes',
      render: (html) => {
        html.find('.roll-info.engagement').css('display', 'none')
        html.find('[name="roll-type"]').on('change', (event) => {
          html.find('.roll-info.engagement').css('display', 'none')
          if (event.currentTarget.value === 'engagement') {
            html.find('.roll-info.engagement').css('display', 'block')
          }
        })
      },
    },
    { height: 'auto' },
  )
  dialog.render(true)
}
