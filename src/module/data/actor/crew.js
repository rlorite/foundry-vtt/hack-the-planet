import ClockField from '../fields/clock.js'
import HoldField from '../fields/hold.js'

export class CrewData extends foundry.abstract.DataModel {
  static defineSchema() {
    const fields = foundry.data.fields
    return {
      base: new fields.StringField(),
      reputation: new fields.SchemaField({
        name: new fields.StringField(),
        value: new fields.NumberField({ initial: 0 }),
        max: new fields.NumberField({ initial: 12 }),
        turfs: new fields.NumberField({ initial: 0 }),
      }),
      tier: new fields.NumberField({ initial: 0 }),
      hold: new HoldField(),
      huntingGround: new fields.StringField(),
      experience: new fields.NumberField({ initial: 0 }),
      coins: new fields.SchemaField({
        max: new fields.NumberField({ initial: 4 }),
        value: new fields.NumberField({ initial: 2 }),
      }),
      vault: new fields.SchemaField({
        max: new fields.NumberField({ initial: 4 }),
        enabled: new fields.NumberField({ initial: 0 }),
        value: new fields.NumberField({ initial: 2 }),
      }),
      heat: new fields.NumberField({ initial: 0 }),
      wanted: new fields.NumberField({ initial: 0 }),
      fuel: new fields.SchemaField({
        disabled: new fields.BooleanField({ initial: true }),
        value: new fields.NumberField({ initial: 0 }),
        max: new fields.NumberField({ initial: 2 }),
      }),
      clocks: new fields.ArrayField(new ClockField()),
    }
  }

  get members() {
    return game.actors.filter(
      (actor) => actor.type === 'character' && actor.system.crew?.id === this.parent.id,
    )
  }
}
