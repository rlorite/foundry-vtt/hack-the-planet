import ClockField from '../fields/clock.js'

const {
  StringField,
  SchemaField,
  NumberField,
  ObjectField,
  BooleanField,
  ForeignDocumentField,
  ArrayField,
} = foundry.data.fields

/**
 * @property {string} alias
 * @property {string} look
 * @property {string} root
 * @property {string} background
 * @property {string} backgroundDetails
 * @property {string} vice
 * @property {string} vicePurveyor
 * @property {object} stress
 * @property {number} stress.current
 * @property {number} stress.max
 * @property {number} stress.maxDefault
 * @property {object} trauma
 * @property {number} trauma.current
 * @property {number} trauma.max
 * @property {number} trauma.maxDefault
 * @property {object} trauma.list
 * @property {number} healingClock
 * @property {object} experience
 * @property {object} experience.playbook
 * @property {number} experience.playbook.current
 * @property {number} experience.playbook.max
 * @property {object} experience.stress
 * @property {number} experience.stress.current
 * @property {number} experience.stress.max
 * @property {object} coins
 * @property {number} coins.current
 * @property {number} coins.stashed
 * @property {number} loadout
 * @property {number} loadLevel
 * @property {object} harm
 * @property {object} harm.light
 * @property {string} harm.light.one
 * @property {string} harm.light.two
 * @property {object} harm.medium
 * @property {string} harm.medium.one
 * @property {string} harm.medium.two
 * @property {object} harm.heavy
 * @property {string} harm.heavy.one
 * @property {object} harm.deadly
 * @property {string} harm.deadly.one
 * @property {object} armorUses
 * @property {boolean} armorUses.armor
 * @property {boolean} armorUses.heavy
 * @property {boolean} armorUses.special
 * @property {HTPActor} crew
 * @property {HTPActor} crew
 * @property {ClockField[]} clocks
 */
export class CharacterData extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    return {
      alias: new StringField({ nullable: false, initial: '' }),
      look: new StringField({ nullable: false, initial: '' }),
      root: new StringField({ nullable: false, initial: '' }),
      background: new StringField({ nullable: false, initial: '' }),
      backgroundDetails: new StringField({ nullable: false, initial: '' }),
      vice: new StringField({ nullable: false, initial: '' }),
      vicePurveyor: new StringField({ nullable: false, initial: '' }),
      stress: new SchemaField({
        current: new NumberField({ initial: 0 }),
        max: new NumberField({ initial: 9 }),
        maxDefault: new NumberField({ initial: 9 }),
      }),
      trauma: new SchemaField({
        current: new NumberField({ initial: 0 }),
        max: new NumberField({ initial: 4 }),
        maxDefault: new NumberField({ initial: 4 }),
        list: new ObjectField({ initial: {} }),
      }),
      healingClock: new NumberField({ initial: 0, max: 4 }),
      experience: new SchemaField({
        playbook: new SchemaField({
          current: new NumberField({ initial: 0 }),
          max: new NumberField({ initial: 8 }),
        }),
        stress: new SchemaField({
          current: new NumberField({ initial: 0 }),
          max: new NumberField({ initial: 8 }),
        }),
      }),
      coins: new SchemaField({
        current: new NumberField({ initial: 0 }),
        stashed: new NumberField({ initial: 0 }),
      }),
      loadout: new NumberField({ initial: 0 }),
      loadLevel: new NumberField({ initial: CONFIG.HTP.LOAD_LEVELS.NORMAL.max }),
      harm: new SchemaField({
        light: new SchemaField({
          one: new StringField({ initial: '' }),
          two: new StringField({ initial: '' }),
        }),
        medium: new SchemaField({
          one: new StringField({ initial: '' }),
          two: new StringField({ initial: '' }),
        }),
        heavy: new SchemaField({
          one: new StringField({ initial: '' }),
        }),
        deadly: new SchemaField({
          one: new StringField({ initial: '' }),
        }),
      }),
      armorUses: new SchemaField({
        armor: new BooleanField({ initial: false }),
        heavy: new BooleanField({ initial: false }),
        special: new BooleanField({ initial: false }),
      }),
      actions: CharacterData.actions(),
      attributes: CharacterData.attributes(),
      crew: new ForeignDocumentField(foundry.documents.BaseActor),
      clocks: new ArrayField(new ClockField()),
    }
  }

  static migrateData(source) {
    if (foundry.utils.hasProperty(source, 'crew.id')) {
      source.crew = source.crew.id
    }
    if ('armor-uses' in source) {
      source.armorUses = source['armor-uses']
    }
    if ('healing-clock' in source) {
      source.healingClock = source['healing-clock'] || 0
    }
    if ('load_level' in source) {
      source.loadLevel = parseInt(source.load_level)
    }
    return super.migrateData(source)
  }

  /**
   *
   * @param {HTPActor} actor
   * @returns {Promise<HTPActor>}
   */
  async addCrew(actor) {
    if (actor.type !== 'crew') throw new Error('Solo se pueden asociar bandas a este actor')
    if (this.crew)
      return ui.notifications.warn(
        'Ya estás asociado a una banda. Elimina la asociación antes de hacer otra',
      )
    return this.parent.update({
      'system.crew': actor.id,
    })
  }

  static actions() {
    const fields = foundry.data.fields
    const actions = {}
    for (const action of Object.keys(CONFIG.HTP.ACTIONS)) {
      actions[action] = new fields.SchemaField({
        label: new fields.StringField({ required: true, initial: CONFIG.HTP.ACTIONS[action] }),
        value: new fields.NumberField({ initial: 0 }),
        initial: new fields.NumberField({ initial: 0 }),
        cyberware: new fields.BooleanField({ initial: false }),
      })
    }
    return new fields.SchemaField(actions)
  }

  static attributes() {
    const fields = foundry.data.fields
    const attributes = {}
    for (const attribute of Object.keys(CONFIG.HTP.ATTRIBUTES)) {
      attributes[attribute] = new fields.SchemaField({
        label: new fields.StringField({
          required: true,
          initial: CONFIG.HTP.ATTRIBUTES[attribute].label,
        }),
        exp: new fields.NumberField({ initial: 0 }),
        expMax: new fields.NumberField({ initial: 6 }),
        actions: new fields.ArrayField(new fields.StringField(), {
          initial: CONFIG.HTP.ATTRIBUTES[attribute].actions,
        }),
      })
    }
    return new fields.SchemaField(attributes)
  }

  await

  async _onUpdate(changed, options, userId) {
    if (!foundry.utils.hasProperty(changed, 'system.experience.stress.current')) return

    if (
      changed.system.experience.stress.current === this.experience.stress.max &&
      this.stress.max <= CONFIG.HTP.MAX_STRESS
    ) {
      await Dialog.confirm({
        title: game.i18n.localize('HTP.Dialogs.TrainStress.Title'),
        content: game.i18n.localize('HTP.Dialogs.TrainStress.Content'),
        defaultYes: false,
        yes: async () => {
          await this.parent.update({
            'system.stress.max': this.stress.max + 1,
            'system.experience.stress.current': 0,
          })
        },
      })
    }
  }
}
