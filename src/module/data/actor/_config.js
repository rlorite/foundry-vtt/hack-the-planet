import { CharacterData } from './character.js'
import { CrewData } from './crew.js'

export default {
  character: CharacterData,
  crew: CrewData,
}
