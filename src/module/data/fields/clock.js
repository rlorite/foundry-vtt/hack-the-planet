const fields = foundry.data.fields

export default class ClockField extends fields.SchemaField {
  constructor(options) {
    super(
      {
        title: new fields.StringField(),
        segments: new fields.NumberField({ choices: CONFIG.HTP.CLOCKS.sizes, initial: 6 }),
        value: new fields.NumberField({ initial: 0 }),
        deleted: new fields.BooleanField({ required: false, initial: false }),
      },
      options,
    )
  }
}
