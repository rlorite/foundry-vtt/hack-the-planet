export default class HoldField extends foundry.data.fields.StringField {
  constructor(options) {
    super({
      initial: 'strong',
      choices: ['weak', 'strong'],
      ...options,
    })
  }
}
