import { FactionData } from './faction.js'
import PlaybookData from './playbook.js'
import CohortData from './cohort.js'
import VehicleData from './vehicle.js'
import AbilityData from './ability.js'
import ItemData from './item.js'
import CrewUpgradeData from './crew_upgrade.js'
import CrewAbilityData from './crew_ability.js'
import CrewTypeData from './crew_type.js'
import FriendData from './friend.js'

export default {
  faction: FactionData,
  playbook: PlaybookData,
  cohort: CohortData,
  vehicle: VehicleData,
  ability: AbilityData,
  item: ItemData,
  crew_upgrade: CrewUpgradeData,
  crew_ability: CrewAbilityData,
  crew_type: CrewTypeData,
  friend: FriendData,
}
