const fields = foundry.data.fields

/**
 * @typedef {object} EdgeFlawData
 * @property {string} key
 * @property {boolean} selected
 */

/**
 * @property {string} description
 * @property {string} cohort_type
 * @property {string} gang_type
 * @property {EdgeFlawData[]} edges
 * @property {EdgeFlawData[]} flaws
 * @property {number} harm
 * @property {boolean} armor
 * @property {boolean} elite
 * @property {string} expert_type
 */
export default class CohortData extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    return {
      description: new fields.HTMLField(),
      cohort_type: new fields.StringField({
        initial: 'Gang',
        choices: Object.keys(CONFIG.HTP.COHORT.types),
      }),
      gang_type: new fields.StringField({
        initial: Object.keys(CONFIG.HTP.COHORT.gangs)[0],
        choices: Object.keys(CONFIG.HTP.COHORT.gangs),
      }),
      edges: new fields.ArrayField(
        new fields.SchemaField({
          key: new fields.StringField(),
          selected: new fields.BooleanField({ required: false, initial: false }),
        }),
        { initial: Object.keys(CONFIG.HTP.COHORT.edge_list).map((key) => ({ key })) },
      ),
      flaws: new fields.ArrayField(
        new fields.SchemaField({
          key: new fields.StringField(),
          selected: new fields.BooleanField({ required: false, initial: false }),
        }),
        { initial: Object.keys(CONFIG.HTP.COHORT.flaw_list).map((key) => ({ key })) },
      ),
      harm: new fields.NumberField({ initial: 0 }),
      armor: new fields.BooleanField({ initial: false }),
      elite: new fields.BooleanField({ initial: false }),
      expert_type: new fields.StringField(),
    }
  }

  prepareDerivedData() {
    let quality = 0
    let scale = 0
    this.active_edges = this.edges
      .filter((edge) => edge.selected)
      .map((edge) => ({
        label: CONFIG.HTP.COHORT.edge_list[edge.key].label,
        description: CONFIG.HTP.COHORT.edge_list[edge.key].description,
      }))
    this.active_flaws = this.flaws
      .filter((flaw) => flaw.selected)
      .map((flaw) => ({
        label: CONFIG.HTP.COHORT.flaw_list[flaw.key].label,
        description: CONFIG.HTP.COHORT.flaw_list[flaw.key].description,
      }))
    // const system = {
    //   ...this.system,
    //   active_edges: Object.values(this.system.edges_list || {}).filter(edge => edge.selected),
    //   active_flaws: Object.values(this.system.flaws_list || {}).filter(edge => edge.selected)
    // }
    const actor = this.parent.actor
    if (!(actor && actor.system)) return

    switch (this.cohort_type) {
      case 'Gang':
        scale = parseInt(actor.system.tier)
        quality = parseInt(actor.system.tier)
        break
      case 'Expert':
        scale = 1
        quality = parseInt(actor.system.tier) + 1
        break
    }
    this.scale = scale
    this.quality = quality
  }

  static migrateData(source) {
    if (Object.prototype.hasOwnProperty.call(source, 'cohort')) {
      source.cohort_type = source.cohort
    }
    if (Object.prototype.hasOwnProperty.call(source, 'edges_list') && !source.edges.length) {
      source.edges = Object.keys(source.edges_list).map((key) => ({
        key,
        selected: source.edges_list[key].selected,
      }))
    }
    if (Object.prototype.hasOwnProperty.call(source, 'flaws_list') && !source.flaws.length) {
      source.flaws = Object.keys(source.flaws_list).map((key) => ({
        key,
        selected: source.flaws_list[key].selected,
      }))
    }
    return super.migrateData(source)
  }
}
