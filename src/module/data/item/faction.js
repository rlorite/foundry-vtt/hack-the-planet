import ClockField from '../fields/clock.js'
import HoldField from '../fields/hold.js'

const fields = foundry.data.fields

/**
 * @property {string} singleName
 * @property {number} tier
 */
export class FactionData extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    return {
      description: new fields.HTMLField({ required: true, nullable: true }),
      notes: new fields.HTMLField({ required: false }),
      singleName: new fields.StringField({ initial: '' }),
      type: new fields.StringField(),
      tier: new fields.NumberField({ initial: 0 }),
      hold: new HoldField(),
      status: new fields.NumberField({ min: -3, max: 3, initial: 0 }),
      goals: new fields.ArrayField(new ClockField(), { initial: [{}, {}] }),
      turf: new fields.StringField(),
      notables: new fields.HTMLField(),
      assets: new fields.HTMLField(),
      quirks: new fields.HTMLField(),
      allies: new fields.StringField(),
      enemies: new fields.StringField(),
      situation: new fields.HTMLField(),
    }
  }

  static migrateData(source) {
    if (foundry.utils.hasProperty(source, 'status.value')) {
      source.status = source.status.value
    }
    const goals = []

    for (const goal of Object.values(source.goals)) {
      if (
        Object.prototype.hasOwnProperty.call(goal, 'goal') &&
        !Object.prototype.hasOwnProperty.call(goal, 'title')
      ) {
        goal.title = goal.goal
      }
      if (
        Object.prototype.hasOwnProperty.call(goal, 'max') &&
        !Object.prototype.hasOwnProperty.call(goal, 'segments')
      ) {
        goal.segments = goal.max
      }
      delete goal.goal
      delete goal.max
      goals.push(goal)
    }
    source.goals = goals
    if (
      foundry.utils.hasProperty(source, 'single_name') &&
      !foundry.utils.hasProperty(source, 'singleName')
    ) {
      source.singleName = source.single_name
    }
    return super.migrateData(source)
  }

  _onUpdate(changed, options, userId) {
    const tier = foundry.utils.getProperty(changed, 'system.tier')
    const name = foundry.utils.getProperty(changed, 'system.singleName')

    if (name || tier) {
      this.parent.update({
        name: `${this.singleName} [${this.tier}]`,
      })
    }
  }
}
