const field = foundry.data.fields

export default class FriendData extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    return {
      hints: new field.StringField(),
      description: new field.HTMLField(),
      playbook: new field.StringField(),
      disposition: new field.StringField({ blank: true, choices: ['friendly', 'enemy'] }),
    }
  }
}
