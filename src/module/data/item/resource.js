const fields = foundry.data.fields

export default class ResourceData extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    return {
      description: new fields.HTMLField(),
      position: new fields.NumberField({ initial: 1, min: 1, max: 15, integer: true }),
      acquired: new fields.BooleanField({ required: false, initial: false }),
      base: new fields.BooleanField({ required: false, initial: false }),
      turf: new fields.BooleanField({ required: false, initial: false }),
      connects: new fields.ArrayField(
        new fields.StringField({ choices: Object.keys(CONFIG.HTP.RESOURCE.connections) }),
      ),
    }
  }
}
