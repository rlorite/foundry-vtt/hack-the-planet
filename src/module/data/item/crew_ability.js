import AbilityData from './ability.js'

const fields = foundry.data.fields
export default class CrewAbilityData extends AbilityData {
  static defineSchema() {
    return {
      ...super.defineSchema(),
      starting_ability: new fields.BooleanField({ required: false, initial: false }),
    }
  }
}
