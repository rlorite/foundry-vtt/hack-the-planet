import ResourceData from './resource.js'

const fields = foundry.data.fields
export default class CrewTypeData extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    return {
      tagline: new fields.StringField(),
      description: new fields.HTMLField(),
      experience_clues: new fields.StringField(),
      hunting_grounds: new fields.ArrayField(new fields.StringField(), {
        initial: ['', '', '', ''],
      }),
      resources: new fields.ArrayField(
        new fields.SchemaField({
          ...ResourceData.defineSchema(),
          name: new fields.StringField(),
        }),
      ),
      starting_upgrades: new fields.ArrayField(new fields.ObjectField()),
      friends: new fields.ArrayField(new fields.ObjectField()),
    }
  }
}
