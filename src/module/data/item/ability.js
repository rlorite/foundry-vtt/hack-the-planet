const fields = foundry.data.fields

export default class AbilityData extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    return {
      single_name: new fields.StringField(),
      playbook: new fields.StringField(),
      description: new fields.HTMLField(),
      extra: new fields.HTMLField(),
      purchased: new fields.BooleanField({ required: false, initial: false }),
    }
  }
}
