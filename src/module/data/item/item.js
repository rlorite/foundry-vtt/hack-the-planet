const fields = foundry.data.fields
export default class ItemData extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    return {
      description: new fields.HTMLField(),
      playbook: new fields.StringField(),
      single_name: new fields.StringField(),
      load: new fields.NumberField({ initial: 0, integer: true, min: 0 }),
      max_load: new fields.NumberField({ initial: 0, integer: true, min: 0 }),
      equipped: new fields.BooleanField({ required: false, initial: false }),
    }
  }

  static migrateData(source) {
    if (isNaN(source.max_load)) source.max_load = 0
    if (isNaN(source.load)) source.load = 0
    super.migrateData(source)
  }
}
