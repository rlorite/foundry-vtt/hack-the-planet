const fields = foundry.data.fields

export default class CrewUpgradeData extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    return {
      single_name: new fields.StringField(),
      playbook: new fields.StringField(),
      description: new fields.HTMLField(),
      price: new fields.NumberField({ initial: 1, integer: true, min: 1 }),
      purchased: new fields.NumberField({ initial: 0, integer: true, min: 0 }),
      starting_upgrade: new fields.BooleanField({ required: false, initial: false }),
    }
  }
}
