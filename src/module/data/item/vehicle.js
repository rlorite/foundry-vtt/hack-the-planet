const fields = foundry.data.fields

/**
 * @typedef {object} EdgeFlawData
 * @property {string} key
 * @property {boolean} selected
 */

/**
 * @property {string} description
 * @property {EdgeFlawData[]} edges
 * @property {EdgeFlawData[]} flaws
 * @property {number} harm
 * @property {boolean} armor
 * @property {boolean} fuel
 */
export default class VehicleData extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    return {
      description: new fields.HTMLField(),
      edges: new fields.ArrayField(
        new fields.SchemaField({
          key: new fields.StringField(),
          selected: new fields.BooleanField({ required: false, initial: false }),
        }),
        { initial: Object.keys(CONFIG.HTP.VEHICLES.edge_list).map((key) => ({ key })) },
      ),
      flaws: new fields.ArrayField(
        new fields.SchemaField({
          key: new fields.StringField(),
          selected: new fields.BooleanField({ required: false, initial: false }),
        }),
        { initial: Object.keys(CONFIG.HTP.VEHICLES.flaw_list).map((key) => ({ key })) },
      ),
      harm: new fields.NumberField({ initial: 0 }),
      armor: new fields.BooleanField({ initial: false }),
      fuel: new fields.BooleanField({ required: false, initial: false }),
    }
  }

  prepareDerivedData() {
    this.active_edges = this.edges
      .filter((edge) => edge.selected)
      .map((edge) => ({
        label: CONFIG.HTP.VEHICLES.edge_list[edge.key].label,
        description: CONFIG.HTP.VEHICLES.edge_list[edge.key].description,
      }))
    this.active_flaws = this.flaws
      .filter((flaw) => flaw.selected)
      .map((flaw) => ({
        label: CONFIG.HTP.VEHICLES.flaw_list[flaw.key].label,
        description: CONFIG.HTP.VEHICLES.flaw_list[flaw.key].description,
      }))
    this.scale = 1
    if (this.parent.actor) {
      this.quality = this.parent.actor.system.tier
    }
  }

  static migrateData(source) {
    if (Object.prototype.hasOwnProperty.call(source, 'edges_list') && !source.edges?.length) {
      source.edges = Object.keys(source.edges_list).map((key) => ({
        key,
        selected: source.edges_list[key].selected,
      }))
    }
    if (Object.prototype.hasOwnProperty.call(source, 'flaws_list') && !source.flaws?.length) {
      source.flaws = Object.keys(source.flaws_list).map((key) => ({
        key,
        selected: source.flaws_list[key].selected,
      }))
    }
    return super.migrateData(source)
  }
}
