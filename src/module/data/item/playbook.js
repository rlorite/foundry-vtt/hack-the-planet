const fields = foundry.data.fields

/**
 * @property {object} base_actions
 * @property {number} base_actions.trace
 * @property {number} base_actions.study
 * @property {number} base_actions.survey
 * @property {number} base_actions.modify
 * @property {number} base_actions.finesse
 * @property {number} base_actions.ghost
 * @property {number} base_actions.combat
 * @property {number} base_actions.skirmish
 * @property {number} base_actions.control
 * @property {number} base_actions.sway
 * @property {number} base_actions.hack
 * @property {number} base_actions.network
 */
export default class PlaybookData extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    return {
      description: new fields.HTMLField(),
      tagline: new fields.StringField(),
      experience: new fields.StringField(),
      friends: new fields.ArrayField(new fields.ObjectField()),
      base_actions: new fields.SchemaField(PlaybookData.baseActions()),
    }
  }

  static baseActions() {
    const schema = {}
    Object.keys(CONFIG.HTP.ACTIONS).forEach(
      (action) => (schema[action] = new fields.NumberField({ initial: 0 })),
    )
    return schema
  }

  async _onUpdate(changed, options, userId) {
    const effectIds = this.parent.getEmbeddedCollection('ActiveEffect').map((effect) => effect._id)
    await this.parent.deleteEmbeddedDocuments('ActiveEffect', effectIds)

    const changes = []
    for (const [action, value] of Object.entries(this.base_actions)) {
      if (parseInt(value)) {
        changes.push({
          key: `system.actions.${action}.initial`,
          value: parseInt(value),
          mode: CONST.ACTIVE_EFFECT_MODES.OVERRIDE,
        })
      }
    }
    await ActiveEffect.create(
      {
        name: this.parent.name,
        changes,
        origin: this.parent.uuid,
        transfer: true,
      },
      { parent: this.parent },
    )
  }
}
