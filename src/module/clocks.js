export default class Clocks {
  constructor(object) {
    this.object = object
    this.isInputShown = false
  }

  addOne(event) {
    if (!this.isInputShown) {
      let { max, value, property, index } = this.extract(event.currentTarget.dataset)
      value++
      if (value > max) {
        value = 0
      }
      return this.update(property, index, value)
    }
    this.hideInput(event)
  }

  showInput(event) {
    const input = event.currentTarget.querySelector('input')
    input.style.display = 'block'
    input.focus()
    this.isInputShown = true
  }

  hideInput(event) {
    const input = event.currentTarget.querySelector('input')
    input.style.display = 'none'
    this.isInputShown = false
  }

  addExpression(event) {
    const clock = event.currentTarget.closest('button.clock')
    let { max, value, property, index } = this.extract(clock.dataset)
    if (event.key === 'Enter') {
      const regex = /^([+-]?)(\d)$/
      const match = event.currentTarget.value.match(regex)
      if (match) {
        const [, operator, increment] = match
        switch (operator) {
          case '+':
            value += Number(increment)
            break
          case '-':
            value -= Number(increment)
            break
          default:
            value = Number(increment)
        }
        value = Math.max(0, value)
        if (value > max) {
          value = value % max
        }
        this.update(property, index, value)
      }
    }
  }

  extract(node) {
    return {
      max: Number(node.max),
      value: Number(node.value),
      property: node.name,
      index: Number(node.index),
    }
  }

  update(property, index, value) {
    const clocks = foundry.utils.getProperty(this.object, property)
    let updated = value
    if (Array.isArray(clocks)) {
      updated = clocks
      updated[index].value = value
    }
    const update = {}
    update[property] = updated
    return this.object.update(update)
  }

  static bind(html, object) {
    const clock = new Clocks(object)
    html.find('button.clock').on('click', (event) => clock.addOne(event))
    html.find('button.clock').on('contextmenu', (event) => clock.showInput(event))
    html.find('button.clock input').on('keydown', (event) => clock.addExpression(event))
    html.find('button.clock input').on('click', (event) => event.stopPropagation())
  }
}
