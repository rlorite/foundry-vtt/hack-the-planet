export class HTPActiveEffectSheet extends ActiveEffectConfig {
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ['sheet', 'active-effect-sheet'],
      template: 'systems/hack-the-planet/templates/sheets/active-effect-sheet.hbs',
      width: 560,
      height: 'auto',
      tabs: [{ navSelector: '.tabs', contentSelector: 'form', initial: 'details' }],
    })
  }

  async getData(options) {
    const context = await super.getData(options)
    return {
      ...context,
      applicable: this.object.getFlag('hack-the-planet', 'applicable'),
      level: this.object.getFlag('hack-the-planet', 'level'),
      isLevelableEffect: this.object.levelable,
      isTransferableToPjs: this.object.transferable,
    }
  }
}
