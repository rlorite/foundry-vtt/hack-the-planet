export class HTPActiveEffect extends ActiveEffect {
  static async onManageActiveEffect(event, owner) {
    // if (owner.isOwned) return ui.notifications.warn(game.i18n.localize('HTP.EffectWarning'))
    event.preventDefault()
    const selector = event.currentTarget.closest('.effect')
    let effect =
      selector && selector.dataset.effectId ? owner.effects.get(selector.dataset.effectId) : null
    switch (event.currentTarget.dataset.action) {
      case 'create':
        effect = await super.create(
          {
            label: 'New effect',
            icon: 'systems/hack-the-planet/assets/icons/Icon.3_13.png',
            origin: owner.uuid,
          },
          { parent: owner },
        )
        return effect.sheet.render(true)
      case 'edit':
        return effect.sheet.render(true)
      case 'delete':
        return effect.delete()
      case 'toggle':
        return effect.update({ disabled: !effect.data.disabled })
    }
  }

  get active() {
    let active = true
    if (this.levelable) {
      const minLevel = this.getFlag('hack-the-planet', 'level')
      active = this.parent.level >= minLevel
    }
    if (this.transferable) active = this.getFlag('hack-the-planet', 'fromCharacter') || false
    switch (this.parent.type) {
      case 'ability':
        active = this.parent.system.starting_ability || this.parent.system.purchased
    }
    return active && !this.disabled && !this.isSuppressed
  }

  getFlag(scope, key) {
    const flag = super.getFlag(scope, key)
    if (!flag && Object.prototype.hasOwnProperty.call(this.flags, key)) return this.flags[key]
    return flag
  }

  get levelable() {
    return CONFIG.HTP.LEVELABLE_ITEM_TYPES.includes(this.parent.type)
  }

  get transferable() {
    return CONFIG.HTP.TRANSFERABLE_ITEM_TYPES.includes(this.parent.type)
  }

  isApplicableTo(actor_type) {
    if (this.transferable) {
      return actor_type === this.getFlag('hack-the-planet', 'applicable')
    }
    return true
  }
}
