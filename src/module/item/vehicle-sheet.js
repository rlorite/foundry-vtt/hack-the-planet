import { HTPItemSheet } from './item-sheet.js'

export default class HTPVehicleSheet extends HTPItemSheet {
  async getData(options) {
    const context = await super.getData(options)

    context.edgeList = CONFIG.HTP.VEHICLES.edge_list
    context.flawList = CONFIG.HTP.VEHICLES.flaw_list
    context.harms = CONFIG.HTP.COHORT.harms

    return context
  }
}
