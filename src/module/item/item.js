import { HTPHelpers } from '../helpers.js'

export class HTPItem extends Item {
  get level() {
    if (this.type === 'crew_upgrade') {
      return this.system.purchased
    }
    return 0
  }

  /** @override */
  async _preCreate(data, options, user) {
    await super._preCreate(data, options, user)

    let removeItems = []
    if (user.id === game.user.id) {
      const actor = this.parent ? this.parent : null
      if (actor?.documentName === 'Actor') {
        removeItems = HTPHelpers.removeDuplicatedItemType(data, actor)
      }
      if (removeItems.length !== 0) {
        await actor.deleteEmbeddedDocuments('Item', removeItems)
      }
    }
  }

  _onUpdate(changed, options, userId) {
    super._onUpdate(changed, options, userId)
    this.system._onUpdate?.(changed, options, userId)
  }

  /* override */
  prepareData() {
    super.prepareData()
    if (['crew_upgrade', 'faction', 'ability', 'crew_ability', 'item'].includes(this.type)) {
      if (this.system.single_name === '') {
        this.system.single_name = this.name
      }
    }

    if (this.type === 'faction') {
      if (!this.system.goal_1_clock_value) {
        this.system.goal_1_clock_value = 0
      }
      if (this.system.goal_1_clock_max === 0) {
        this.system.goal_1_clock_max = 4
      }
      if (!this.system.goal_2_clock_value) {
        this.system.goal_2_clock_value = 0
      }
      if (this.system.goal_2_clock_max === 0) {
        this.system.goal_2_clock_max = 4
      }
      this.system.size_list = this.system.goals.map((goal) =>
        HTPHelpers.createListOfClockSizes(CONFIG.HTP.CLOCKS.sizes, goal.segments, goal.segments),
      )
    }
  }
}
