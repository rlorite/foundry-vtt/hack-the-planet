import { PlaybookSheet } from './playbook-sheet.js'

export class CrewTypeSheet extends PlaybookSheet {
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ['hack-the-planet', 'sheet', 'item', 'crew-type'],
      width: 600,
      height: 650,
      dragDrop: [{ dragSelector: '.item-list .item', dropSelector: null }],
    })
  }

  activateListeners(html) {
    super.activateListeners(html)
    html.find('.resource-delete').on('click', this._onDeleteResource.bind(this))
    html.find('.upgrade .item-delete').on('click', this._onDeleteUpgrade.bind(this))
  }

  async _onDropItem(event, data) {
    const itemData = await super._onDropItem(event, data)
    switch (itemData.type) {
      case 'resource':
        return this._onDropResource(itemData)
      case 'crew_upgrade':
        return this._onDropCrewUpgrade(itemData)
    }
  }

  async _onDropResource(resource) {
    for (let position = 1; position <= 15; position++) {
      if (!this.item.system.resources.find((res) => res.position === position)) {
        resource.system.position = position
        break
      }
    }
    if (resource.system.base) {
      resource.system.position = 8
    }
    new Dialog({
      title: `${game.i18n.localize('Add')} ${resource.name}`,
      content: await renderTemplate('systems/hack-the-planet/templates/dialogs/add-resource.hbs', {
        resource,
        connections: CONFIG.HTP.RESOURCE.connections,
      }),
      buttons: {
        one: {
          icon: '<i class="fas fa-check"></i>',
          label: game.i18n.localize('Add'),
          callback: async (html) => await this.createResource(html, resource),
        },
        two: {
          icon: '<i class="fas fa-times"></i>',
          label: game.i18n.localize('Cancel'),
          callback: () => false,
        },
      },
      default: 'two',
    }).render(true)
  }

  async createResource(html, base_resource) {
    const position = parseInt($(html).find('input[name=resource_position]').val())
    const exist = this.item.system.resources.find((resource) => resource.position === position)
    if (exist) {
      const err = new Error(game.i18n.localize('HTP.ResourcePositionError'))
      return ui.notifications.error(err.message)
    }
    const connections = document.querySelectorAll('input[name=resource_connects]')
    const connects = Array.prototype.filter
      .call(connections, (connection) => connection.checked)
      .map((connection) => connection.value)
    const resources = this.item.system.resources
    resources.push({
      ...base_resource.system,
      name: base_resource.name,
      acquired: false,
      position,
      connects,
    })
    this._tabs[0].activate('resources')
    await this.item.update({
      'system.resources': resources.sort((a, b) => parseInt(a.position) - parseInt(b.position)),
    })
  }

  async _onDeleteResource(event) {
    event.preventDefault()
    const resourceId = event.currentTarget.closest('.resource').dataset.resourceId
    const resources = this.item.system.resources.filter(
      (resource) => resource.position !== resourceId,
    )
    return await this.item.update({
      'system.resources': resources,
    })
  }

  async _onDeleteUpgrade(event) {
    event.preventDefault()
    const upgradeId = event.currentTarget.closest('.upgrade').dataset.itemId
    const upgrades = this.item.system.starting_upgrades.filter(
      (upgrade) => upgrade._id !== upgradeId,
    )
    return await this.item.update({
      'system.starting_upgrades': upgrades,
    })
  }

  async _onDropCrewUpgrade(upgrade) {
    this._tabs[0].activate('upgrades')
    const upgrades = this.item.system.starting_upgrades
    const exists = upgrades.find((up) => up._id === upgrade._id)
    if (!exists) {
      upgrades.push(upgrade)
      return await this.item.update({ 'system.starting_upgrades': upgrades })
    }
    return ui.notifications.error(game.i18n.localize('HTP.CrewUpgradeAlreadyExists'))
  }
}
