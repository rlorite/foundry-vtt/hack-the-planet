import { HTPItemSheet } from './item-sheet.js'

export class PlaybookSheet extends HTPItemSheet {
  async _onDropItem(event, data) {
    const itemData = await super._onDropItem(event, data)
    switch (itemData.type) {
      case 'friend':
        await this._onDropFriend(itemData)
        break
    }
    return itemData
  }

  activateListeners(html) {
    super.activateListeners(html)
    html.find('.friend-delete').on('click', this._onDeleteFriend.bind(this))
  }

  async _onDropFriend(friend) {
    const friends = this.item.system.friends
    const exists = friends.some((f) => f._id === friend._id)

    if (!exists) {
      friends.push(friend)
    }

    await this.item.update({
      'system.friends': friends,
    })
  }

  async _onDeleteFriend(event) {
    event.preventDefault()
    const friendId = event.currentTarget.closest('.friend').dataset.itemId
    const friends = this.item.system.friends.filter((friend) => friend._id !== friendId)
    return await this.item.update({
      'system.friends': friends,
    })
  }
}
