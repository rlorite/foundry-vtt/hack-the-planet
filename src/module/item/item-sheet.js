/**
 * Extend the basic ItemSheet
 * @extends {ItemSheet}
 */
import { HTPHelpers } from '../helpers.js'
import { HTPActiveEffect } from '../active-effect/index.js'
import Clocks from '../clocks.js'

export class HTPItemSheet extends ItemSheet {
  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ['hack-the-planet', 'sheet', 'item'],
      width: 560,
      height: 'auto',
      dragDrop: [{ dragSelector: '.item-list .item', dropSelector: null }],
      tabs: [
        { navSelector: '.tabs', contentSelector: '.tab-content', initial: 'basic-data' },
        { navSelector: '.sheet-tabs', contentSelector: '.sheet-body', initial: 'description' },
      ],
    })
  }

  /** @override */
  get template() {
    const path = 'systems/hack-the-planet/templates/items'
    const simple_item_types = ['background', 'heritage', 'vice', 'crew_reputation']
    let template_name = `${this.item.type}`

    if (simple_item_types.indexOf(this.item.type) >= 0) {
      template_name = 'simple'
    }

    return `${path}/${template_name}.hbs`
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html)

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return

    Clocks.bind(html, this.item)
    html
      .find('.effect-control')
      .on('click', (event) => HTPActiveEffect.onManageActiveEffect(event, this.item))
  }

  /** @override */
  async getData(options) {
    const context = {
      ...super.getData(options),
      system: this.item.system,
      isGM: game.user.isGM,
      effects: this.item.effects,
      isOwned: this.item.isOwned,
      enriched: {},
    }

    if (Object.prototype.hasOwnProperty.call(this.item.system, 'description')) {
      context.enriched = {
        description: await TextEditor.enrichHTML(this.item.system.description),
      }
    }
    if (Object.prototype.hasOwnProperty.call(this.item.system, 'extra')) {
      context.enriched = {
        ...context.enriched,
        extra: await TextEditor.enrichHTML(this.item.system.extra),
      }
    }

    let playbook_type = 'playbook'
    if (['crew_ability', 'crew_upgrade'].includes(this.item.type)) {
      playbook_type = 'crew_type'
    }

    const playbooks = await HTPHelpers.getAllItemsByType(playbook_type)
    context.playbooks = playbooks
      .map((playbook) => playbook.name)
      .reduce((a, v) => ({ ...a, [v]: v }), {
        '': '---' + game.i18n.localize('HTP.ChoosePlaybook') + '---',
      })

    context.holds = { weak: 'HTP.Weak', strong: 'HTP.Strong' }

    return context
  }

  async _updateObject(event, formData) {
    if (['crew_upgrade', 'ability', 'item', 'crew_ability'].includes(this.item.type)) {
      const playbook = formData['system.playbook']
      formData.name = formData['system.single_name']
      if (playbook !== '') {
        formData.name = `[${playbook}] ${formData.name}`
      }
    }
    return super._updateObject(event, formData)
  }

  async _onDrop(event) {
    // Try to extract the data
    let data
    try {
      data = JSON.parse(event.dataTransfer.getData('text/plain'))
    } catch (err) {
      return false
    }
    switch (data.type) {
      case 'Item': {
        return this._onDropItem(event, data)
      }
    }
  }

  async _onDropItem(event, data) {
    const item = await Item.implementation.fromDropData(data)
    return item.toObject()
  }
}
