import { HTPItemSheet } from './item-sheet.js'

export default class HTPCohortSheet extends HTPItemSheet {
  async getData(options) {
    const context = await super.getData(options)

    context.cohortTypes = CONFIG.HTP.COHORT.types
    context.gangTypes = CONFIG.HTP.COHORT.gangs
    context.edgeList = CONFIG.HTP.COHORT.edge_list
    context.flawList = CONFIG.HTP.COHORT.flaw_list
    context.harms = CONFIG.HTP.COHORT.harms

    return context
  }
}
