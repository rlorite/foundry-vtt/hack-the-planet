import rangeHelper from 'handlebars-helper-range'

export default () => {
  Handlebars.registerHelper('range', rangeHelper)

  Handlebars.registerHelper('ifAnd', function (cond1, cond2, options) {
    if (cond1 && cond2) {
      return options.fn(this)
    }
    return options.inverse(this)
  })
  // Is the value Turf side.
  Handlebars.registerHelper('is_turf_side', function (value, options) {
    if (['left', 'right', 'top', 'bottom'].includes(value)) {
      return options.fn(this)
    } else {
      return options.inverse(this)
    }
  })

  // Multiboxes.
  Handlebars.registerHelper('multiboxes', function (selected, initial, options) {
    if (arguments.length < 3) {
      options = initial
      initial = 0
    }

    let html = options.fn(this)

    if (typeof selected !== 'undefined') {
      if (selected !== false) {
        if (selected < initial) {
          selected = initial
        }
        const escapedValue = RegExp.escape(Handlebars.escapeExpression(selected))
        const rgx = new RegExp(' value="' + escapedValue + '"')
        html = html.replace(rgx, '$& checked="checked"')
      }
    }
    return html
  })

  // Trauma Counter
  Handlebars.registerHelper('traumacounter', function (selected, options) {
    const html = options.fn(this)

    let count = 0
    for (const trauma in selected) {
      if (selected[trauma] === true) {
        count++
      }
    }

    if (count > 4) count = 4

    const rgx = new RegExp(' value="' + count + '"')
    return html.replace(rgx, '$& checked="checked"')
  })

  // NotEquals handlebar.
  Handlebars.registerHelper('noteq', (a, b, options) => {
    return a !== b ? options.fn(this) : ''
  })

  // ReputationTurf handlebar.
  Handlebars.registerHelper('repturf', (turfs_amount, options) => {
    let html = options.fn(this)
    let turfs_amount_int = parseInt(turfs_amount)

    // Can't be more than 6.
    if (turfs_amount_int > 6) {
      turfs_amount_int = 6
    }

    for (let i = 13 - turfs_amount_int; i <= 12; i++) {
      const rgx = new RegExp(' value="' + i + '"')
      html = html.replace(rgx, '$& disabled="disabled"')
    }
    return html
  })

  Handlebars.registerHelper('crew_vault_coins', (max_coins, options) => {
    let html = options.fn(this)
    for (let i = 1; i <= max_coins; i++) {
      html +=
        '<input type="radio" id="crew-coins-vault-' +
        i +
        '" name="data.vault.value" value="' +
        i +
        '"><label for="crew-coins-vault-' +
        i +
        '"></label>'
    }

    return html
  })

  Handlebars.registerHelper('crew_experience', (actor, options) => {
    let html = options.fn(this)
    for (let i = 1; i <= 10; i++) {
      html += `<input type="radio" id="crew-${actor._id}-experience-${i}" name="data.experience" value="${i}" dtype="Radio"><label for="crew-${actor._id}-experience-${i}"></label>`
    }

    return html
  })

  // Enrich the HTML replace /n with <br>
  Handlebars.registerHelper('html', (options) => {
    const text = options.hash.text.replace(/\n/g, '<br />')

    return new Handlebars.SafeString(text)
  })

  // "N Times" loop for handlebars.
  //  Block is executed N times starting from n=1.
  //
  // Usage:
  // {{#times_from_1 10}}
  //   <span>{{this}}</span>
  // {{/times_from_1}}
  Handlebars.registerHelper('times_from_1', function (n, block) {
    let accum = ''
    for (let i = 1; i <= n; ++i) {
      accum += block.fn(i)
    }
    return accum
  })

  // "N Times" loop for handlebars.
  //  Block is executed N times starting from n=0.
  //
  // Usage:
  // {{#times_from_0 10}}
  //   <span>{{this}}</span>
  // {{/times_from_0}}
  Handlebars.registerHelper('times_from_0', function (n, block) {
    let accum = ''
    for (let i = 0; i <= n; ++i) {
      accum += block.fn(i)
    }
    return accum
  })

  // Concat helper
  // https://gist.github.com/adg29/f312d6fab93652944a8a1026142491b1
  // Usage: (concat 'first 'second')
  Handlebars.registerHelper('concat', function () {
    let outStr = ''
    for (const arg in arguments) {
      if (typeof arguments[arg] !== 'object') {
        outStr += arguments[arg]
      }
    }
    return outStr
  })

  /**
   * @inheritDoc
   * Takes label from Selected option instead of just plain value.
   */

  Handlebars.registerHelper('selectOptionsWithLabel', function (choices, options) {
    const localize = options.hash.localize ?? false
    let selected = options.hash.selected ?? null
    const blank = options.hash.blank || null
    selected = selected instanceof Array ? selected.map(String) : [String(selected)]

    // Create an option
    const option = (key, object) => {
      if (localize) object.label = game.i18n.localize(object.label)
      const isSelected = selected.includes(key)
      html += `<option value="${key}" ${isSelected ? 'selected' : ''}>${object.label}</option>`
    }

    // Create the options
    let html = ''
    if (blank) option('', blank)
    Object.entries(choices).forEach((e) => option(...e))

    return new Handlebars.SafeString(html)
  })

  /**
   * Create appropriate Blades clock
   */

  Handlebars.registerHelper('clock', function (parameter_name, type, current_value, uniq_id) {
    let html = ''

    if (current_value === null || current_value === 'null') {
      current_value = 0
    }

    if (parseInt(current_value) > parseInt(type)) {
      current_value = type
    }

    html += `
        <button class="clock" type="button" data-name="${parameter_name}" data-value="${current_value}" data-max="${type}">
            <svg class="clock"><use xlink:href="#clock-${type}"></use></svg>
        </button>`
    // html += `<label class="clock-zero-label" for="clock-0-${uniq_id}}"><i class="fab fa-creative-commons-zero nullifier"></i></label>`;
    // html += `<div id="blades-clock-${uniq_id}" class="blades-clock clock-${type} clock-${type}-${current_value}" style="background-image:url('systems/hack-the-planet/assets/icons/clocks/Progress Clock ${type}-${current_value}.svg');">`;
    //
    // let zero_checked = (parseInt(current_value) === 0) ? 'checked="checked"' : '';
    // html += `<input type="radio" value="0" id="clock-0-${uniq_id}}" name="${parameter_name}" ${zero_checked} data-dtype="Number">`;
    //
    // for (let i = 1; i <= parseInt(type); i++) {
    //   let checked = (parseInt(current_value) === i) ? 'checked="checked"' : '';
    //   html += `
    //     <input type="radio" value="${i}" id="clock-${i}-${uniq_id}" name="${parameter_name}" ${checked}  data-dtype="Number">
    //     <label for="clock-${i}-${uniq_id}"></label>
    //   `;
    // }
    //
    // html += `</div>`;
    return html
  })

  Handlebars.registerHelper(
    'boxDisabled',
    function (currentBox, disabledTrack, disabledBoxes, totalBoxes) {
      return disabledTrack || currentBox > totalBoxes - disabledBoxes ? 'disabled' : ''
    },
  )
}
