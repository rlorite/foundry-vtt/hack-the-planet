import './scss/style.scss'
// Import Modules
import { delegate } from 'tippy.js'
import { preloadHandlebarsTemplates } from './module/templates.js'
import HTPRoll, { simpleRollPopup } from './module/roll.js'
import { HTPHelpers } from './module/helpers.js'
import { HTPItem } from './module/item/item.js'
import { HTPItemSheet } from './module/item/item-sheet.js'
import { HTPActorSheet, HTPCrewSheet, HTPFactionSheet } from './module/actor/index.js'
import { HTPActor } from './module/actor/actor.js'
import { ChatCard } from './module/chat/card.js'
import { CrewTypeSheet } from './module/item/crew-type-sheet.js'
import { HTPActiveEffect, HTPActiveEffectSheet } from './module/active-effect/index.js'
import handlebarsHelpers from './module/handlebars/helpers.js'
import { PlaybookSheet } from './module/item/playbook-sheet.js'
import HTP from './module/config.js'
import ActorDataModels from './module/data/actor/_config.js'
import ItemDataModels from './module/data/item/_config.js'
import HTPCohortSheet from './module/item/cohort-sheet.js'
import HTPVehicleSheet from './module/item/vehicle-sheet.js'

window.HTPHelpers = HTPHelpers
/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */
Hooks.once('init', async function () {
  CONFIG.HTP = HTP

  game.system.bobclocks = {
    sizes: CONFIG.HTP.CLOCKS.sizes,
  }

  delegate('body', {
    target: '[data-tippy-content]',
    allowHTML: true,
    placement: 'bottom',
  })

  CONFIG.Dice.HTPRoll = HTPRoll
  CONFIG.Dice.rolls.push(HTPRoll)

  CONFIG.Item.documentClass = HTPItem
  CONFIG.Item.dataModels = ItemDataModels

  CONFIG.Actor.documentClass = HTPActor
  CONFIG.Actor.dataModels = ActorDataModels

  CONFIG.ActiveEffect.legacyTransferral = false
  CONFIG.ActiveEffect.documentClass = HTPActiveEffect

  // Register sheet application classes
  Actors.unregisterSheet('core', ActorSheet)
  Actors.registerSheet('htp', HTPActorSheet, { types: ['character'], makeDefault: true })
  Actors.registerSheet('htp', HTPCrewSheet, { types: ['crew'], makeDefault: true })
  Actors.registerSheet('htp', HTPFactionSheet, { types: ['factions'], makeDefault: true })
  // Actors.registerSheet("htp", BladesClockSheet, { types: ["\uD83D\uDD5B clock"], makeDefault: true });
  // Actors.registerSheet("htp", BladesNPCSheet, { types: ["npc"], makeDefault: true });
  Items.unregisterSheet('core', ItemSheet)
  Items.registerSheet('htp', HTPItemSheet, { makeDefault: true })
  Items.registerSheet('htp', CrewTypeSheet, { types: ['crew_type'], makeDefault: true })
  Items.registerSheet('htp', PlaybookSheet, { types: ['playbook'], makeDefault: true })
  Items.registerSheet('htp', HTPCohortSheet, { types: ['cohort'], makeDefault: true })
  Items.registerSheet('htp', HTPVehicleSheet, { types: ['vehicle'], makeDefault: true })

  DocumentSheetConfig.registerSheet(
    CONFIG.ActiveEffect.documentClass,
    'core',
    HTPActiveEffectSheet,
    {
      makeDefault: true,
    },
  )

  ChatCard.registerHooks()

  await preloadHandlebarsTemplates()
  handlebarsHelpers()
})

/*
 * Hooks
 */

// getSceneControlButtons
Hooks.on('renderSceneControls', async (app, html) => {
  const diceRoller = $(
    '<li class="scene-control" title="Dice Roll"><i class="fas fa-dice"></i></li>',
  )
  diceRoller.click(async function () {
    await simpleRollPopup()
  })
  if (!foundry.utils.isNewerVersion('9', game.version ?? game.data.version)) {
    html.children().first().append(diceRoller)
  } else {
    html.append(diceRoller)
  }
})
